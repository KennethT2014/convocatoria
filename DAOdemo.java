public class DAOdemo {
 
	public static void main(String[] args) {
		// objeto para manipular el dao
		Nota estudianteDAO = new Nota();
 
		// imprimir los estudiantes
        estudianteDAO.obtenerEstudiantes().forEach(System.out::println);
 
		// obtener un estudiante
        Estudiante estudiante = estudianteDAO.obtenerEstudiantes(0);
        Estudiante.setApellidos("López");
		//actualizar estudiantes
        estudianteDAO.actualizarEstudiante(estudiante);
 
		// imprimir los estudiantes
		System.out.println("*****");
		estudianteDao.obtenerEstudiantes().forEach(System.out::println);
	}
}
