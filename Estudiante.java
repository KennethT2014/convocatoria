public class Estudiante {
	private int id;
	private String carnet;
	private String nombres;
	private String apellidos;
    private String cedula;
    private String telefono;
    private int Notas;
	
		
	public Estudiante() {
		super();
	}
	public Estudiante(int id, String carnet, String nombres, String apellidos, String cedula, String telefono, int Notas) {
		super();
		this.id = id;
		this.carnet = carnet;
		this.nombres = nombres;
		this.apellidos = apellidos;
        this.cedula = cedula;
        this.telefono = telefono;
        this.Notas = Notas;
	}
	public int getId() {
		return id;
	}
	public void getId(int id) {
		this.id = id;
	}

	public String getCarnet() {
		return carnet;
	}
	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

    public String getCedula() {
        return cedula;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getNotas() {
        return Notas;
    }
    public void setNotas(String Notas) {
        this.Notas = Notas;
    }
	
	@Override
	public String toString() {
        return this.getCarnet()+"\n"+this.getNombres()+" "+this.getApellidos()+": "+this.getNotas+"\n"+this.getCedula()+"\n"+this.getTelefono();
	}
}
