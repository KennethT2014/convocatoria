public class Nota implements estudianteDAO {
	
	//lista de tipo estudiantes
	List<Estudiante> Estudiantes;
	
	//inicializar los objetos estudiante y añadirlos a la lista
	public Nota() {
        Estudiante = new ArrayList<>();
		Estudiante Estudiante1 = new Estudiante(001,"2020-1155A","Javier", "Molina", "002-181101-6589V", "84552112", 80);
		Estudiante Estudiante2 = new Estudiante(002,"2020-1156A","Lillian","Álvarez", "002-181103-9540B", "58963247", 90);
		estudiantes.add(Estudiante1);
		estudiantes.add(Estudiante2);
	}
	
	//obtener todos los estudiantes
	@Override
	public List<Estudiante> obtenerEstudiantes() {
		return estudiantes;
	}
	
	//obtener un estudiante por carnet
	@Override
	public Estudiante obtenerEstudiantes(String Carnet) {
		return estudiantes.get(Carnet);
	}
	
	//actualizar un estudiante
	@Override
	public void actualizarEstudiante(Estudiante Estudiante) {
        Estudiante.get(Estudiante.getId()).setCarnet(Estudiante.getCarnet());
        Estudiante.get(Estudiante.getId()).setNombres(Estudiante.getNombres());
        Estudiante.get(Estudiante.getId()).setApellidos(Estudiante.getApellidos());
        Estudiante.get(Estudiante.getId()).setCedula(Estudiante.getCedula());
        Estudiante.get(Estudiante.getId()).setTelefono(Estudiante.getEstudiante());
        Estudiante.get(Estudiante.getId()).setNotas(Estudiante.getNotas());
		System.out.println("Estudiante con id: "+Estudiante.getId()+" actualizado satisfactoriamente");
	}
	
	//eliminar un estudiante por carnet
	@Override
	public void eliminarEstudiante(Estudiante Estudiante) {
        Estudiante.remove(Estudiante.getCarnet());
        System.out.println("Estudiante con Carnet: "+Estudiante.getCarnet()+" elimnado satisfactoriamente");
	}
}
