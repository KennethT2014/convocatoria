public interface estudianteDAO {
	public List<Estudiante> obtenerEstudiante();
	public Estudiante obtenerId(int id);
	public void actualizarId(Estudiante id);
	public void eliminarId(Estudiante id);
}
